============
Installation
============

At the command line::

    $ pip install process-config-drive

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv process-config-drive
    $ pip install process-config-drive