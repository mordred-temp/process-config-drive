===============================
process-config-drive
===============================

Simple program to write static config from config-drive

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/process-config-drive
* Source: http://git.openstack.org/cgit/openstack/process-config-drive
* Bugs: http://bugs.launchpad.net/process-config-drive

Features
--------

* TODO
